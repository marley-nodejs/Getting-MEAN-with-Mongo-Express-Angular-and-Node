# [BOOK BY SIMON HOLMES] Getting MEAN with Mongo, Express, Angular, and Node [ENG, May 1, 2015]

<br/>
Original src: <a href="https://github.com/simonholmes/getting-MEAN" rel="nofollow">here</a>
<br/>

I'm working on Ubuntu in docker container with debian jessie:

    $ lsb_release -a
    Description:	Ubuntu 14.04.5 LTS
    Codename:	trusty

<br/>

    $ docker -v
    Docker version 1.9.1, build a34a1d5


<a href="http://jsdev.org/env/docker/run-container/">How to run docker container for start development</a>  
(If link will not work give me to know about it)

    $ node -v
    v4.5.0

<br/>

    $ npm -v
    2.15.9

___

<br/>

**PLAN:**

    STEP 1: BUILD A STATIC SITE
    STEP 2: DESIGN THE DATA MODEL AND CREATE THE DATABASE
    STEP 3: BUILD OUR REST API
    STEP 4: USE THE API FROM OUR APPLICATION
    STEP 5: EMBELLISH THE APPLICATION
    STEP 6: REFACTOR THE CODE INTO AN ANGULARJS SPA
    STEP 7: ADD AUTHENTICATION




<br/>

### STEP 1: BUILD A STATIC SITE

<br/>

**Creating an Express project**

    # npm install -g nodemon
    # npm install -g express
    # npm install -g express-generator

    $ su - developer
    $ cd /project/
    $ express .
    $ npm install
    $ nodemon start

http://localhost:3000/


<br/>

**Modifying Express for MVC**

![Creating an Express project](/img/step1-pic1.png?raw=true)




<br/><br/>
___

**Marley**

<a href="https://jsdev.org">jsdev.org</a>

email:  
![Marley](http://img.fotografii.org/a3333333mail.gif "Marley")
